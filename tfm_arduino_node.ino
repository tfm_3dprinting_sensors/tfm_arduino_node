#include <ros.h>
#include <std_msgs/Float64.h>

#define VA A0
#define VB A1

#define VA2 A4
#define VB2 A5

int va_value = 0;
int vb_value = 0;

int va_value2 = 0;
int vb_value2 = 0;


ros::NodeHandle node_handle;

std_msgs::Float64 sensor1_msg;
std_msgs::Float64 sensor2_msg;

ros::Publisher sensor1_publisher("/arduino/sensor1/voltage", &sensor1_msg);
ros::Publisher sensor2_publisher("/arduino/sensor2/voltage", &sensor2_msg);

void setup()
{
  pinMode(VA, INPUT);
  pinMode(VB, INPUT);
  pinMode(VA2, INPUT);
  pinMode(VB2, INPUT);

  
  node_handle.initNode();
  node_handle.advertise(sensor1_publisher);
  node_handle.advertise(sensor2_publisher);
}

void loop()
{ 
  va_value = analogRead(VA);
  float va_msg = va_value*(5.0/1023.0);
  vb_value = analogRead(VB);
  float vb_msg = vb_value*(5.0/1023.0);

  sensor1_msg.data = va_msg-vb_msg;

  va_value2 = analogRead(VA2);
  float va_msg2 = va_value2*(5.0/1023.0);
  vb_value2 = analogRead(VB2);
  float vb_msg2 = vb_value2*(5.0/1023.0);

  sensor2_msg.data = va_msg2-vb_msg2;

  sensor1_publisher.publish( &sensor1_msg );
  sensor2_publisher.publish( &sensor2_msg );

  node_handle.spinOnce();
  
  delay(100);
}
