# tfm_arduino_node

:exclamation: Please, to use this repository start with doing the setup defined in https://gitlab.com/tfm_3dprinting_sensors/tfm_workspace_setup.

## Repository structure

This repository contains the code to convert an Arduino UNO board in a ROS node broadcasting sensors data.

## Authors and acknowledgment
Oriol Vendrell

## License
BSD 2-Clause "Simplified" License

## Project status
This project is part of my final master's thesis.
